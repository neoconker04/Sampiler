--[[    Plugin name: Jump Reslover
    Alias folder: None
    Desc: 把跳躍指令還原成原本位置
    Author: Sam
--]]
---@class JumpReslover
local this = { logic_reslove = false }

local jumpPointer, loopBlock, logicBlock, fixJumpPos

function this:init()
    log = require("util.Log")("INFO")
    jumpPointer = require("plugin.decompile.static.block.JumpPointer")
    loopBlock = require("plugin.decompile.static.block.LoopBlock")
    logicBlock = require("plugin.decompile.static.block.LogicBlockv2")
    fixJumpPos = require("plugin.decompile.static.block.FixJumps")
    return {
        id = "JumpReslover",
        name = "跳躍指令還原",
        desc = "把跳躍指令還原成原本位置",
        author = "Sam",
        config = false,
        config_settings = {}
    }
end

local logicOPs = {
    LOADBOOL = true,
    EQ = true,
    LT = true,
    LE = true,
    TEST = true,
    TESTSET = true;
}
local jumpOPs = {
    JMP = true,
    FORPREP = true,
    FORLOOP = true,
    TFORLOOP = true;
}

local sleep = gg.sleep

function this:main()
    local currLevel = Editor.currentLevel
    local info = Editor.protoLevel[currLevel]
    if info.State == Editor.States.DELETED or info.State == Editor.States.EMPTY then
        Toast(Language.err.ListIsEmpty)
        return
    end
    while true do
        local input = Choice({
            "還原邏輯指令: " ..
            Language.other[this.logic_reslove == true and "Yes" or "No"][1],
            "開始還原"
        })
        if not input or input == 0 then
            return
        elseif input == 1 then
            this.logic_reslove = not this.logic_reslove
        elseif input == 2 then
            local targetProto = Editor.cacheProto[currLevel]
            local origInstr = targetProto.instr
            local count = #origInstr
            local curr = 1
            local cleanedInstr = {}
            local called = {}
            local ignore = {}
            -- Init block framework
            local jump_pointer, loop_block, logic_block
            if this.logic_reslove then
                jump_pointer, loop_block = jumpPointer(targetProto), loopBlock(targetProto)
                logic_block = logicBlock(targetProto, loop_block, jump_pointer)
                loop_block = nil
                for i = 1, #logic_block do
                    -- take all logic block's jump out, so that they can ignore
                    ignore[logic_block[i]._start + 1] = true
                    if logic_block[i].type == "logic_if_else" then
                        ignore[logic_block[i]._else] = true
                    elseif logic_block[i].type == "while" then
                        ignore[logic_block[i]._end] = true
                    end
                end
                logic_block = nil
            end
            Alert("Orig total: "..count)
            Alert("Ignore:"..tostring(ignore))
            -- error(0)
            local currInstr, currCODE, currSBX
            repeat
                if not called[curr] then called[curr] = 0 end
                called[curr] = called[curr] + 1
                if called[curr] > 1 then
                    error(called)
                end
                currInstr = origInstr[curr]
                currCODE, currSBX = currInstr[1], currInstr[2]['sBx']
                if ignore[curr] then
                    ;
                elseif currCODE == "JMP" then
                    curr = curr + currSBX + 1
                    goto skip
                elseif logicOPs[currCODE] then
                    for i = 0, 2 do
                        cleanedInstr[#cleanedInstr+1] = origInstr[curr+i]
                    end
                    curr = curr + 3
                    goto skip
                end
                cleanedInstr[#cleanedInstr+1] = currInstr
                curr = curr + 1
                ::skip::
            until curr > count
            if Alert("Total instr: "..#cleanedInstr,"YES") == 1 then
                Editor.cacheProto[currLevel].instr = this.logic_reslove and fixJumpPos(cleanedInstr, jump_pointer) or cleanedInstr
            end
            Toast("Code has resorted")
            break
        end
    end
    return "close_plugin_UI"
end

return this