---@type Declaration
local Declaration = require("plugin.decompile.pseudo.Declaration")

---@class RegisterState
local RegisterState = {
    last_written = 1;
    last_read = -1;
    read_count = 0;
    isTemporary = false;
    isLocal = false;
    isRead = false;
    isWritten = false;
}

function RegisterState:new(obj)
    obj = obj or {

    }
    setmetatable(obj, self)
    self.__index = self
    return obj
end

---@class RegisterStates
local RegisterStates = {
    registers = 0,
    lines = 0
}

local set_index;
set_index = function(t, key)
    if not rawget(t, key) then
        local new_t = rawset(t, key, setmetatable({}, { __index = set_index }))
        return rawget(new_t, key)
    end
end

local contains_v = function(t, value)
    for k, v in ipairs(t) do
        if v == value then
            return true
        end
    end
end

local remove_v = function(t, value)
    for k, v in ipairs(t) do
        if v == value then
            return table.remove(t, k)
        end
    end
end

function RegisterStates:new(registers, lines)
    ---@type RegisterState
    local obj = {}
    setmetatable(obj, self)
    self.__index = self
    obj.registers = registers
    obj.lines = lines
    ---@type RegisterState[][]
    obj.states = {}
    for line = 0, lines do
        for register = 0, registers do
            obj:get(register, line)
        end
    end
    return obj
end

---@field public get RegisterStates
---@param register number
---@param line number
---@return RegisterState
function RegisterStates:get(register, line)
    if not self.states[line - 1] then
        self.states[line - 1] = {}
    end
    if not self.states[line - 1][register] then
        self.states[line - 1][register] = RegisterState:new()
    end
    return self.states[line - 1][register]
end

---@field public setWritten void
---@param register number
---@param line number
function RegisterStates:setWritten(register, line)
    self:get(register, line).isWritten = true
    self:get(register, line + 1).last_written = line
end

---@field public setRead void
---@param register number
---@param line number
function RegisterStates:setRead(register, line)
    self:get(register, line).isRead = true
    self:get(register, self:get(register, line).last_written).read_count =    self:get(register, self:get(register, line).last_written).read_count + 1
    self:get(register, self:get(register, line).last_written).last_read = line;
end

---@field public setLocalRead void
---@param register number
---@param line number
function RegisterStates:setLocalRead(register, line)
    for r = 0, register do
        self:get(r, self:get(r, line).last_written).isLocal = true
    end
end

---@field public setLocalWrite void
---@param register_min number
---@param register_max number
---@param line number
function RegisterStates:setLocalWrite(register_min, register_max, line)
    for r = 0, register_min do
        self:get(r, self:get(r, line).last_written).isLocal = true
    end
    for r = register_min, register_max do
        self:get(r, line).isLocal = true
    end
end

---@field public setTemporaryRead void
---@param register number
---@param line number
function RegisterStates:setTemporaryRead(register, line)
    for r = 0, register do
        self:get(r, self:get(r, line).last_written).isTemporary = true
    end
end

---@field public setTemporaryWrite void
---@param register_min number
---@param register_max number
---@param line number
function RegisterStates:setTemporaryWrite(register_min, register_max, line)
    for r = 0, register_min do
        self:get(r, self:get(r, line).last_written).isTemporary = true
    end
    for r = register_min, register_max do
        self:get(r, line).isTemporary = true
    end
end

---@field public nextLine void
---@param line number
function RegisterStates:nextLine(line)
    if line + 1 < self.lines then
        for r = 0, self.registers do
            if self:get(r, line).last_written > self:get(r, line + 1).last_written then
                self:get(r, line + 1).last_written = self:get(r, line).last_written
            end
        end
    end
end

---@field private isConstantReference boolean
---@param value number
---@return boolean
local function isConstantReference(const, value)
    if const[-value] then
        return true
    elseif value < 0 then
        -- TODO Raise warning
        return false
    end
    return false
end

---@field public process table
---@param d Prototype
---@param args number
---@param registers number
---@return table
local function process(d, args, registers)
    args = args <= 200 and args or 200
    registers = registers <= 200 and registers or 200
    -- EXCEPTION: They won't change Lua's registers/args
    local lc = 0
    local code = d.instr
    ---@type RegisterStates
    local states = RegisterStates:new(registers, #code)
    local skip = {}
    ---@field private case boolean
    ---@type fun(...):boolean
    ---@return boolean
    local case

    for line = 1, #code do
        states:nextLine(line)
        if skip[line] then goto continue end
        case = function(...)
            local test_case = { ... }
            for i = 1, #test_case do
                if code[line][1] == test_case[i] then
                    return true
                end
            end
            return false
        end
        local OP, A, B, C = code[line][1], code[line][2].A, code[line][2].B, code[line][2].C
        --[[            加载参数
            LOADK LOADKX
            WRITE R(A)

            一元函数
            MOVE UNM BNOT NOT LEN
            WRITE R(A) READ R(B)

            二元函数
            ADD SUB MUL MOD POW DIV IDIV BAND BOR BXOR SHL SHR
            WRITE R(A) READ RK(B) RK(C)

            表格处理
            GETTABLE
            WRITE R(A) READ R(B) RK(C)
            SETTABLE
            WRITE R(A) READ RK(B) RK(C)
            NEWTABLE
            WRITE R(A)
            SELF
            WRITE R(A+1) READ R(B)
            WRITE R(A) READ R(B) RK(C)
            SETLIST
            WRITE R(A) READ (B > 0) { i++=1->B; R(A+i) } else { VAR > 200 ? VAR = 200; i++=1->VAR-A; R(A+i) }

            元组处理
            LOADNIL
            WRITE { i++=A->B; R(i) }
            CONCAT
            WRITE R(A) READ { i++=B->C; R(i) }
            CALL
            READ R(A) { i++=A+1->A+B-1; R(i) } WRITE { i++=A->A+C-2; R(i) }
            RETURN
            READ { i++=A->A + B - 2; R(i) }
            VARARG
            WRITE B == 0 ? B = VAR; { i++=A->A+B-2; R(i) }
            TAILCALL
            READ R(A) { i++=A+1->A+B-1; R(i) }
            TFORCALL
            // A as a function, A+1 and A+2 are A(A+1, A+2)
            READ[TEMP] { i++=A->A+2; R(i) }
            WRITE[TEMP] { i++=A+3->A+2+C; R(i) } End PC = pc - 1

            与外部局部变量的交互
            GETUPVAL
            WRITE R(A)
            SETUPVAL
            READ R(A)
            GETTABUP
            WRITE R(A)
            SETTABUP
            READ RK(B) RK(C)

            逻辑函数
            LOADBOOL
            WRITE R(A)
            EQ LT LE
            READ RK(B) RK(C)
            TEST
            READ R(A)
            TESTSET
            READ R(B) WRITE R(A)

            分支、循环和闭包
            JMP
            // Start from R-1, to ? (Max local start from R-1)
            A > 0 ? { i++=R-1->#?; endpc R(i) }
            FORLOOP
            READ[TEMP] { i++=A->A+2; R(i) } End PC = pc - 1
            WRITE[TEMP] R(A+3) End PC = pc
            FORPREP
            READ[TEMP] { i++=A->A+2; R(i) } Start PC = pc - 1
            WRITE[TEMP] R(A+3) Start PC = pc
            TFORLOOP
            READ[TEMP] R(A+1)
            WRITE[TEMP] R(A)
            WRITE/READ[TEMP] { A -= 2; i++=A->A+2; R(i) } End PC = pc Start PC = (pc + sBx - 1) := startpc == JMP ? startpc &&
            WRITE/READ[TEMP] { A -= 2; i++=A+3->A+2+C; R(i) } Start PC = startpc-1 : (line start)
            CLOSURE
            WRITE R(A)

            整理: 最终case
            // 太麻烦了, 我宁可不整理
        --]]
        if case("MOVE") then
            states:setWritten(A, line)
            states:setRead(B, line)
            if (A < B) then
                states:setLocalWrite(A, A, line)
            elseif (B > A) then
                states:setLocalRead(B, line)
            end
        elseif case("LOADK", "LOADKX") then
            -- WRITE R(A)
            states:setWritten(A, line)
        elseif case("UNM", "BNOT", "NOT", "LEN") then
            -- WRITE R(A) READ R(B)
            states:setWritten(A, line)
            states:setRead(B, line)
        elseif case("ADD", "SUB", "MUL", "MOD", "POW", "DIV", "IDIV", "BAND", "BOR", "BXOR", "SHL", "SHR") then
            -- WRITE R(A) READ RK(B) RK(C)
            states:setWritten(A, line)
            if not isConstantReference(d.const, B) then states:setRead(B, line) end
            if not isConstantReference(d.const, C) then states:setRead(C, line) end
        elseif case("GETTABLE") then
            -- WRITE R(A) READ R(B) RK(C)
            states:setWritten(A, line)
            states:setRead(B, line)
            if not isConstantReference(d.const, C) then states:setRead(C, line) end
        elseif case("SETTABLE") then
            -- WRITE R(A) READ RK(B) RK(C)
            states:setWritten(A, line)
            if not isConstantReference(d.const, B) then states:setRead(B, line) end
            if not isConstantReference(d.const, C) then states:setRead(C, line) end
        elseif case("NEWTABLE") then
            -- WRITE R(A)
            states:setWritten(A, line)
        elseif case("SELF") then
            -- WRITE R(A+1) READ R(B)
            -- WRITE R(A) READ R(B) RK(C)
            states:setWritten(A + 1, line)
            states:setRead(B, line)
            states:setWritten(A, line)
            states:setRead(B, line)
            if not isConstantReference(d.const, C) then states:setRead(C, line) end
        elseif case("SETLIST") then
            -- WRITE R(A) READ (B > 0) { i++=1->B; R(A+i) } else { VAR > 200 ? VAR = 200; i++=1->VAR-A; R(A+i) }
            states:setWritten(A, line)
            if B > 0 then
                for i = 1, B do
                    states:setTemporaryRead(A + i, line)
                end
            else
                local var = registers
                if var > 200 then
                    var = 200
                end
                for i = 1, var - A do
                    states:setTemporaryRead(A + i, line)
                end
            end
        elseif case("LOADNIL") then
            -- WRITE { i++=A->B; R(i) }
            for i = A, B do
                states:setRead(i, line)
            end
        elseif case("CONCAT") then
            --WRITE R(A) READ { i++=B->C; R(i) }
            states:setWritten(A, line)
            for i = B, C do
                states:setTemporaryRead(i, line)
            end
        elseif case("CALL") then
            --READ R(A) { i++=A+1->A+B-1; R(i) }
            --WRITE { i++=A->A+C-2; R(i) }
            states:setRead(A, line)
            for i = A + 1, A + B - 1 do
                states:setRead(i, line)
            end
            states:setTemporaryWrite(A, A + C - 2, line)
            if C >= 2 then
                local nline = line + 1
                local register = A + C - 2
                while (register >= A and nline <= #code) do
                    if code[nline][1] == "MOVE" and code[nline][2].B == register then
                        states:setWritten(code[nline][2].A, nline)
                        states:setRead(code[nline][2].B, nline)
                        states:setLocalWrite(code[nline][2].A, code[nline][2].A, nline)
                        skip[nline - 1] = true
                    end
                    register = register - 1
                    nline = nline + 1
                end
            end
        elseif case("RETURN") then
            --READ { i++=A->A + B - 2; R(i) }
            for i = A, A + B - 2 do
                states:setRead(i, line)
            end
        elseif case("VARARG") then
            --WRITE B == 0 ? B = VAR; { i++=A->A+B-2; R(i) }
            if B == 0 then
                local var = registers
                if var > 200 then
                    var = 200
                end
                B = var
            end
            for i = A, A + B - 2 do
                states:setWritten(i, line)
            end
        elseif case("TAILCALL") then
            --READ R(A) { i++=A+1->A+B-1; R(i) }
            states:setRead(A, line)
            for i = A + 1, A + B - 1 do
                states:setRead(i, line)
            end
        elseif case("TFORCALL") then
            --READ[TEMP] { i++=A->A+2; R(i) }
            --WRITE[TEMP] { i++=A+3->A+2+C; R(i) } End PC = pc - 1
            for i = A, A + 2 do
                states:setTemporaryRead(i, line)
            end
            for i = A + 3, A + 2 + C do
                states:setTemporaryWrite(i, i, line)
                -- End PC = pc - 1
            end
        elseif case("GETUPVAL") then
            --WRITE R(A)
            states:setWritten(A, line)
        elseif case("SETUPVAL") then
            --READ R(A)
            states:setRead(A, line)
        elseif case("GETTABUP") then
            --WRITE R(A)
            states:setWritten(A, line)
        elseif case("SETTABUP") then
            --READ RK(B) RK(C)
            if not isConstantReference(d.const, B) then states:setRead(B, line) end
            if not isConstantReference(d.const, C) then states:setRead(C, line) end
        elseif case("LOADBOOL") then
            --WRITE R(A)
            states:setWritten(A, line)
        elseif case("EQ", "LT", "LE") then
            --READ RK(B) RK(C)
            if not isConstantReference(d.const, B) then states:setRead(B, line) end
            if not isConstantReference(d.const, C) then states:setRead(C, line) end
        elseif case("TEST") then
            --READ R(A)
            states:setRead(A, line)
        elseif case("TESTSET") then
            --READ R(B) WRITE R(A)
            states:setRead(B, line)
            states:setWritten(A, line)
        elseif case("JMP") then
            --A > 0 ? { i++=R-1->#?; endpc R(i) }
            if A > 0 then
                -- TODO
            end
        elseif case("FORLOOP") then
            --READ[TEMP] { i++=A->A+2; R(i) } End PC = pc - 1
            --WRITE[TEMP] R(A+3) End PC = pc
            for i = A, A + 2 do
                states:setTemporaryRead(i, line)
                -- End PC = pc - 1
            end
            states:setTemporaryWrite(A + 3, A + 3, line)
            -- End PC = pc
        elseif case("FORPREP") then
            --READ[TEMP] { i++=A->A+2; R(i) } Start PC = pc - 1
            --WRITE[TEMP] R(A+3) Start PC = pc
            for i = A, A + 2 do
                states:setTemporaryRead(i, line)
                -- Start PC = pc - 1
            end
            states:setTemporaryWrite(A + 3, A + 3, line)
            -- Start PC = pc
        elseif case("TFORLOOP") then
            --READ[TEMP] R(A+1)
            --WRITE[TEMP] R(A)
            --WRITE/READ[TEMP] { A -= 2; i++=A->A+2; R(i) } End PC = pc Start PC = (pc + sBx - 1) := startpc == JMP ? startpc &&
            --WRITE/READ[TEMP] { A -= 2; i++=A+3->A+2+C; R(i) } Start PC = startpc-1 : (line start)
            states:setTemporaryRead(A + 1, line)
            states:setTemporaryWrite(A, A, line)
            -- TODO
            -- TODO
        elseif case("CLOSURE") then
            --WRITE R(A)
            local f_id = d.func[-code[line][2].Bx].id
            local f = Editor.cacheProto[f_id]
            for i = 1, #f.upvalue do
                local upvalue = f.upvalue[i]
                if upvalue.instack then
                    states:setLocalRead(upvalue.position, line)
                end
            end
            states:setWritten(A, line)
        end
        :: continue ::
    end
    for register = 0, registers - 1 do
        states:setWritten(register, 1);
    end
    for line = 1, #code do
        for register = 0, registers - 1 do
            ---@type RegisterState
            local s = states:get(register, line);
            if s.isWritten then
                if s.read_count >= 2 or (line >= 2 and s.read_count == 0) then
                    states:setLocalWrite(register, register, line);
                end
            end
        end
    end
    for line = 1, #code do
        for register = 0, registers - 1 do
            ---@type RegisterState
            local s = states:get(register, line);
            if (s.isWritten and s.isTemporary) then
                ---@type table<number, number|boolean>
                local ancestors = {}
                for read = 0, registers - 1 do
                    ---@type RegisterState
                    local r = states:get(read, line);
                    if r.isRead and r.isLocal then
                        ancestors[#ancestors + 1] = read
                    end
                end
                local pline = 0;
                for pline_i = line - 1, 1, -1 do
                    pline = pline_i
                    local any_written = false
                    for pregister = 0, registers - 1 do
                        if (states:get(pregister, pline_i).isWritten and contains_v(ancestors, pregister)) then
                            any_written = true
                            remove_v(ancestors, pregister)
                        end
                    end
                    if not any_written then
                        break
                    end
                    for pregister = 0, registers - 1 do
                        ---@type RegisterState
                        a = states:get(pregister, pline_i);
                        if (a.isRead and not a.isLocal) then
                            ancestors[#ancestors + 1] = pregister;
                        end
                    end
                end
                for i = 0, #ancestors do
                    local ancestor = ancestors[i]
                    if (pline >= 1) then
                        if not ancestor then
                            ancestor = 0
                        end
                        states:setLocalRead(ancestor, pline);
                    end
                end
            end
        end
    end
    -- [[ DEBUG
    for register = 0, registers - 1 do
        for line = 1, #code do
            ---@type RegisterState
            local s = states:get(register, line)
            if s.isWritten or line == 1 then
                print("WRITE reg:" .. register .. " line:" .. line .. " last read: " .. s.last_read)
                if s.isLocal then print("  LOCAL") end
                if s.isTemporary then print("  TEMPORARY") end
                print("  READ_COUNT " .. s.read_count)
                print("+==========+")
            end
        end
    end
    --]]
    local declList = {}
    for register = 0, registers - 1 do
        local id = "L"
        local isLocal, isTemporary, isArg = false, false, false
        local read, written, start = 0, 0, 0
        if (register < args) then
            isLocal = true;
            isArg = true;
            id = "A";
        end
        if not isLocal and not isTemporary then
            for line = 1, #code do
                ---@type RegisterState
                local state = states:get(register, line)
                if state.isLocal then
                    isTemporary = false
                    isLocal = true
                end
                if state.isTemporary then
                    start = line + 1
                    isTemporary = true
                end
                if state.isRead then
                    written = 0
                    read = read + 1
                end
                if state.isWritten then
                    if written > 0 and read == 0 then
                        isTemporary = false
                        isLocal = true
                    end
                    read = 0
                    written = written + 1
                end
            end
        end
        if not isLocal and not isTemporary then
            if read >= 2 or read == 0 and written ~= 0 then
                isLocal = true
            end
        end
        if isLocal then
            ---@type Declaration
            local decl = Declaration:new(id .. register .. "_" .. lc, start, #code, isArg)
            lc = lc + 1
            decl.register = register
            declList[#declList + 1] = decl
        end
    end
    -- [[ DEBUG
    ---@param decl Declaration
    for i = 1, #declList do
        local decl = declList[i]
        -- "decl: " + decl.name + " " + decl.begin + " " + decl.end
        print("decl: " .. decl.name .. " as " .. decl.register)
    end
    --]]
    -- if Alert("Stop?","OK") == 1 then os.exit() end
    return declList
end

return process