--[[    OP Handling
--]]
local this = {}
local cache;
local RK = function(iType)
    if tonumber(iType) > 0 then
        return "r" .. iType
    else
        local const = nil;
        if cache.proto.const[-iType] then
            const = "CONST( " .. tostring(cache.proto.const[-iType][1]) .. " )"
        else
            const = "(null)"
        end
        return const
    end
end
local CODE = {
    MOVE = function(A, B)
        if A == B then
            return
        end
        local out = "r" .. A .. " = r" .. B;
        DecompOutput.add(out)
    end, -- MOVE | iAB
    LOADK = function(A, Bx)
        local out = "r" .. A .. " = " .. RK(Bx)
        DecompOutput.add(out)
    end, -- LOADK | iABx
    LOADKX = function(A)

    end, -- LOADKX | iA
    LOADBOOL = function(A, B, C)

    end, -- LOADBOOL | iABC
    LOADNIL = function(A, B)
        local out = "local "
        for i = A, B do
            out = out .. "r" .. i
            if i ~= B then
                out = out .. ","
            end
        end
        out = out .. ";"
        DecompOutput.add(out)
    end, -- LOADNIL | iAB
    GETUPVAL = function(A, B)
        local out = "r" .. A .. " = (UPVAL" .. B .. ")"
        DecompOutput.add(out)
    end, -- GETUPVAL | iAB
    GETTABUP = function(A, B, C)

    end, -- GETTABUP | iABC
    GETTABLE = function(A, B, C)
        local out = "r" .. A .. " = r" .. B .. "[" .. RK(C) .. "]"
        DecompOutput.add(out)
    end, -- GETTABLE | iABC
    SETTABUP = function(A, B, C)

    end, -- SETTABUP | iABC
    SETUPVAL = function(A, B)

    end, -- SETUPVAL | iAB
    SETTABLE = function(A, B, C)
        local out = "r" .. A .. "[" .. RK(B) .. "] = " .. RK(C)
        DecompOutput.add(out)
    end, -- SETTABLE | iABC
    NEWTABLE = function(A, B, C)
        local out = "r" .. A .. " = { };"
        DecompOutput.add(out)
    end, -- NEWTABLE | iABC
    SELF = function(A, B, C)

    end, -- SELF | iABC
    ADD = function(A, B, C)
        local out = "r" .. A .. " = " .. RK(B) .. " + " .. RK(C)
        DecompOutput.add(out)
    end, -- ADD | iABC
    SUB = function(A, B, C)
        local out = "r" .. A .. " = " .. RK(B) .. " - " .. RK(C)
        DecompOutput.add(out)
    end, -- SUB | iABC
    MUL = function(A, B, C)
        local out = "r" .. A .. " = " .. RK(B) .. " * " .. RK(C)
        DecompOutput.add(out)
    end, -- MUL | iABC
    DIV = function(A, B, C)
        local out = "r" .. A .. " = " .. RK(B) .. " / " .. RK(C)
        DecompOutput.add(out)
    end, -- DIV | iABC
    MOD = function(A, B, C)
        local out = "r" .. A .. " = " .. RK(B) .. " % " .. RK(C)
        DecompOutput.add(out)
    end, -- MOD | iABC
    POW = function(A, B, C)
        local out = "r" .. A .. " = " .. RK(B) .. " ^ " .. RK(C)
        DecompOutput.add(out)
    end, -- POW | iABC
    UNM = function(A, B)
        local out = "r" .. A .. " = - r" .. B
        DecompOutput.add(out)
    end, -- UNM | iAB
    NOT = function(A, B)
        local out = "r" .. A .. " = not r" .. B
        DecompOutput.add(out)
    end, -- NOT | iAB
    LEN = function(A, B)
        local out = "r" .. A .. " = # r" .. B
        DecompOutput.add(out)
    end, -- LEN | iAB
    CONCAT = function(A, B, C)
        local out = "r" .. A .. " = r" .. B .. " .. r" .. C
        DecompOutput.add(out)
    end, -- CONCAT | iABC
    JMP = function(A, sBx)
        local dist = cache.pc + sBx + 1
        -- DecompOutput.add("JMP: To pc","["..dist.."]")
        return true, dist
    end, -- JMP | iAsBx
    EQ = function(A, B, C)

    end, -- EQ | iABC
    LT = function(A, B, C)

    end, -- LT | iABC
    LE = function(A, B, C)

    end, -- LE | iABC
    TEST = function(AC)

    end, -- TEST | iAC
    TESTSET = function(A, B, C)

    end, -- TESTSET | iABC
    CALL = function(A, B, C)

    end, -- CALL | iABC
    TAILCALL = function(A, B, C)

    end, -- TAILCALL | iABC
    RETURN = function(A, B)

    end, -- RETURN | iAB
    FORLOOP = function(A, sBx)

    end, -- FORLOOP | iAsBx
    FORPREP = function(A, sBx)

    end, -- FORPREP | iAsBx
    TFORCALL = function(AC)

    end, -- TFORCALL | iAC
    TFORLOOP = function(A, sBx)

    end, -- TFORLOOP | iAsBx
    SETLIST = function(A, B, C)

    end, -- SETLIST | iABC
    CLOSURE = function(A, Bx)

    end, -- CLOSURE | iABx
    VARARG = function(A, B)

    end, -- VARARG | iAB
    EXTRAARG = function(Ax)

    end, -- EXTRAARG | iAx
}

function this:handleOP(instr, ENVcache)
    cache = ENVcache
    local OPName = instr[1]
    local args = instr[2]
    local A = tostring(args['A'] or args['Ax'])
    local B = tostring(args['B'] or args['Bx'] or args['sBx'])
    local C = tostring(args['C'])
    if OPName == "UNKNOWN" then
        -- Unable to handle opcode UNKNOWN
        Alert('Unable to handle opcode ' .. tostring(OPName), "")
        return
    end
    return CODE[OPName](A, B, C)
end

function this:init()
    -- this.nameList = Bytecode:getOPNameList() -- index = string
    -- this.numList  = Bytecode:getOPNumList() -- index = number
end

return this