local log = require("util.Log")("INFO")
return function(instr, JumpList)
    local newInstr = {} -- ? 新指令
    local newGuidPos = {} -- ? 储存新指令的JMP
    local queueGuidPos = {} -- ? 暂时未找到target的JMP
    local targetCounter = {} -- ? 计算目标多少request
    local orig = JumpList
    log.debug("正在整理Jump列表以方便应用")
    for i = 1, #JumpList do
        local curr = JumpList[i]
        JumpList[curr.request] = curr.target
        if targetCounter[curr.target] then
            targetCounter[curr.target] = targetCounter[curr.target] + 1
        elseif not targetCounter[curr.target] then
            targetCounter[curr.target] = 1
        end
        JumpList[i] = nil
        -- 会有相同的target，但不会有相同的request
    end
    log.info('Rearranging... (This may take some time)')
    for i, t in pairs(instr) do
        local curr = t
        local id = curr.id
        newGuidPos[id] = #newInstr + 1
        -- * Format: request -> target *
        if JumpList[id] then -- request found
            local target = JumpList[id]
            if newGuidPos[target] then -- 已经存在, OK
                -- default op: JMP
                instr[i][2][2] = -(#newInstr + 1) + newGuidPos[target] - 1
                if targetCounter[target] and targetCounter[target] > 1 then
                    targetCounter[target] = targetCounter[target] - 1
                else
                    targetCounter[target] = nil
                end
                -- log.debug("Curr pos:",#newInstr+1,"To pos:",newGuidPos[target],"("..(-(#newInstr+1) +newGuidPos[target]-1)..","..""..")")
            else -- 放进排队列表中以等候
                queueGuidPos[#newInstr + 1] = target
                -- new instr is waiting for target
            end
        end
        newInstr[#newInstr + 1] = instr[i]
        -- Check queued
        if targetCounter[id] then -- found target
            -- log.debug("id",id,targetCounter[id])
            -- 请求早已被定义，因此直接修改newInstr
            -- * target = id
            for instr_num, request_target in pairs(queueGuidPos) do
                -- will slow down progress, cannot be fixed
                -- log.debug("instr_num",instr_num)
                -- log.debug("request_target",request_target)
                if request_target == id then
                    newInstr[instr_num][2][2] = -- target instr = #newInstr
                    -- reuqest instr = instr_num
                    -instr_num + #newInstr - 1
                    -- log.debug("Curr pos:",instr_num,"To pos:",#newInstr,"("..(-instr_num + #newInstr-1)..","..""..")")
                    -- clean queue
                    queueGuidPos[instr_num] = nil
                    if not targetCounter[id] then
                        log.error("UUID `" .. id ..
                                      "` invaild (Repeated? Already changed?)")
                    end
                    if targetCounter[id] and targetCounter[id] > 1 then
                        targetCounter[id] = targetCounter[id] - 1
                    else
                        targetCounter[id] = nil
                    end
                end
            end
            -- queue end, loop n done.
        end
    end
    log.info('Sort completed')
    if next(targetCounter) then
        log.warn(
            "Some position can't be fixed , usually caused by target missing.")
        -- log.warn(require"inspect"(instr))
        -- log.warn(require"inspect"(newGuidPos))
        -- log.error(require"inspect"(targetCounter))
    end
    return newInstr
end
