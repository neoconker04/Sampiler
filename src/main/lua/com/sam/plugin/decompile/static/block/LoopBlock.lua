local log = require "util.Log"("WARN")
local pc = 2
local blocks = {}
local ignore_block = {}
local globalTree
-- reduce global fetch
local pairs = pairs
--
local getId = function(instr_num)
    return globalTree.instr[instr_num].id
end

local addType = function(type, arg)
    local target = {}
    for name, instr_num in pairs(arg) do
        target[name] = getId(instr_num)
    end
    arg.type = type
    arg.target = target
    blocks[#blocks + 1] = arg
end

local addIgnore = function(instr_num)
    ignore_block[instr_num] = true
end

local pharseNumberLoop = function(instr, B)
    if instr[pc + B] and instr[pc + B][1] == 'FORLOOP' and (pc + B) + instr[pc + B][2].sBx == pc - 1 then
        addIgnore(pc - 1)
        log.info('block类型: number loop')
        -- log.debug('Block size:', B)
        return 'loop_number', {
            _start = pc - 1,
            _end = pc + B
        }
    else
        log.error('X not a vaild block, critical error')
        return error('critical error')
    end
end

local pharseCallLoop = function(instr, B)
    if instr[pc + B + 1] and instr[pc + B + 1][1] == 'TFORLOOP' then
        addIgnore(pc - 1)
        log.info('block类型: call loop')
        -- log.debug(' Block size:', B)
        return 'loop_call', {
            _start = pc - 1,
            _end = pc + B + 1
        }
    else
        log.error('X not a vaild block, critical error')
        return error('critical error')
    end
end

local parseInstr = function(instr)
    blocks = {}
    local maxPC = #instr + 1
    while true do
        local Instr = instr[pc]
        pc = pc + 1
        if pc >= maxPC then
            return
        end
        local OP, B = Instr[1], Instr[2].sBx or 0
        if OP == 'FORPREP' and not ignore_block[pc - 1] then -- Loop
            addType(pharseNumberLoop(instr, B))
        elseif OP == 'JMP' then
            if instr[pc + B] and instr[pc + B][1] == 'TFORCALL' and not ignore_block[pc - 1] then
                addType(pharseCallLoop(instr, B))
            elseif not instr[pc + B] then -- Invaild jump
                log.warn("Invaild Jump position. (Jumped to pc " .. (pc + B) .. ")")
            end
        end
    end
end

local function Init(tree)
    globalTree = tree
    log.info('开始解析loop block')
    parseInstr(tree.instr)
    log.info('解析loop block完成, 总共解析到 ' .. #blocks .. ' 个block(s)')
    -- log.debug(require"inspect"(blocks))
    pc, ignore_block =    2, {}
    local ret = blocks
    blocks = {}
    return ret
end

return Init