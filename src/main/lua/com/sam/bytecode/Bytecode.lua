---@class Bytecode
Bytecode = {}
local OP_Name_Type, OPCodes = nil, {}
local genID = require("util.GUID_Gen")

function Bytecode:init(version)
    OP_Name_Type = require("bytecode.version." .. version)
    for i = 0, 46 do
        ---@param string any OP_NAME
        ---@return number OP_NUM
        OPCodes[next(OP_Name_Type[i])] = i
    end
end

local SIZE, POS = {}, {}
SIZE.C = 9
SIZE.B = 9
SIZE.Bx = 18
SIZE.A = 8
SIZE.Ax = 26
SIZE.OP = 6
POS.OP = 0
POS.A = 6
POS.C = 14
POS.B = 23
POS.Bx = 14
POS.Ax = 6
local mB = 255
local msBx = 131071

local function bias(B)
    if B < 0 then
        return mB - B
    else
        return B
    end
end
local function unbias(B)
    if B > mB then
        return mB - B
    else
        return B
    end
end

-----------------------------------
-- Local define
local EXT = bit and bit.extract or bit32.extract

local bit = bit or bit32

local BAND = bit.band
local BNOT = bit.bnot
local BOR = bit.bor

local SHL = bit.lshift
-----------------------------------
function Bytecode:toOP(raw)
    return EXT(raw, POS.OP, SIZE.OP)
end

-----------------------------------
-- A
-----------------------------------
function Bytecode:toA(raw)
    return EXT(raw, POS.A, SIZE.A)
end

function Bytecode:formA(arg)
    return BAND(SHL(arg, POS.A), BNOT(SHL(-1, POS.A + SIZE.A)))
end

-----------------------------------
-- B
-----------------------------------
function Bytecode:toB(raw)
    return unbias(EXT(raw, POS.B, SIZE.B))
end

function Bytecode:formB(arg)
    return BAND(SHL(bias(arg), POS.B), BNOT(SHL(-1, POS.B + SIZE.B)))
end

-----------------------------------
-- C
-----------------------------------
function Bytecode:toC(raw)
    return unbias(EXT(raw, POS.C, SIZE.C))
end

function Bytecode:formC(arg)
    return BAND(SHL(bias(arg), POS.C), BNOT(SHL(-1, POS.C + SIZE.C)))
end

-----------------------------------
-- Ax
-----------------------------------
function Bytecode:toAx(raw)
    return -EXT(raw, POS.Ax, SIZE.Ax)
end

function Bytecode:formAx(arg)
    return BAND(SHL(-arg, POS.Ax), BNOT(SHL(-1, POS.Ax + SIZE.Ax)))
end

-----------------------------------
-- Bx
-----------------------------------
function Bytecode:toBx(raw)
    return -1 - EXT(raw, POS.Bx, SIZE.Bx)
end

function Bytecode:formBx(arg)
    return BAND(SHL(-1 - arg, POS.Bx), BNOT(SHL(-1, POS.Bx + SIZE.Bx)))
end

-----------------------------------
-- sBx
-----------------------------------
function Bytecode:tosBx(raw)
    return -(msBx - EXT(raw, POS.Bx, SIZE.Bx))
end

function Bytecode:formsBx(arg)
    return BAND(SHL(msBx + arg, POS.Bx), BNOT(SHL(-1, POS.Bx + SIZE.Bx)))
end

-----------------------------------
-- End
-----------------------------------
function Bytecode:getReadable(raw)
    -- 把raw變成OP + 原本的類型(比如iABC)
    local OP = Bytecode:toOP(raw)
    local query = OP_Name_Type[OP]
    if query == nil then -- UNKNOWN
        return { "UNKNOWN", { Ax = Bytecode:toAx(raw) }, OP = OP, type = { 'Ax' } }
    end
    local Name, Type = next(query)
    local arg = {}
    for i = 1, #Type do
        arg[Type[i]] = Bytecode["to" .. Type[i]](Bytecode, raw)
    end
    return { Name, arg, type = Type, id = genID() }
end

function Bytecode:toBinary(nameargs)
    -- 把OP + 類型變成二进制
    local OPName = nameargs[1]
    local OPCode = OPCodes[OPName]
    if not OPCode then
        if nameargs.OP then
            OPCode = nameargs.OP
        else
            print(nameargs)
            error("未知指令 '" .. OPName .. "'")
        end
    end
    local OPType = nameargs.type
    local OPArg = nameargs[2]
    for _, Type in ipairs(OPType) do
        OPCode = BOR(OPCode, Bytecode["form" .. Type](Bytecode, OPArg[Type]))
    end
    return OPCode
end

function Bytecode:changeType(nameargs, targetType)
    local raw = Bytecode:toBinary(nameargs)
    local Type = targetType
    local arg = {}
    for i = 1, #Type do
        arg[Type[i]] = Bytecode["to" .. Type[i]](Bytecode, raw)
    end
    return arg
end

function Bytecode:getOPNumList()
    return OP_Name_Type
end

function Bytecode:getOPNameList()
    return OPCodes
end