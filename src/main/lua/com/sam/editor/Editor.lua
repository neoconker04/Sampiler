---@class Editor
Editor = {
    currentLevel = 0
}

---@class CacheProto
local CacheProto;

---加載區塊, 並拆解區塊讓編輯器可修改
---@param proto Prototype
---@return Editor
function Editor:LoadPrototype(proto)
    -- This should be a temp table, with a level displayed (AKA Level already loaded as cache, but not instr and etc)
    --[[        State: EXIST, DELETED, EMPTY(Instr)
    --]]
    local cacheProto = {} -- All proto that expend as one table
    local protoLevel = {} -- All proto's level as text (Example, "%main" or "[10]") and also State
    Editor.States = { EXIST = "EXIST", DELETED = "DELETED", EMPTY = "EMPTY" }
    local listProto;
    listProto = function(prevLevel, currProto)
        local currLev = #protoLevel + 1
        cacheProto[currLev] = currProto
        protoLevel[currLev] = { DEFAULT = currLev, SSTOOL = Editor:getPrefix(currProto.level, prevLevel),
        State = Editor.States.EXIST }
        if #currProto.instr == 0 then
            protoLevel[currLev].State = Editor.States.EMPTY
        end
        if #currProto.func > 0 then -- More proto (Sub proto)
            for i = 1, #currProto.func do
                local id = listProto(protoLevel[currLev].SSTOOL, currProto.func[i])
                currProto.func[i] = {
                    id = id
                }
            end
        end
        return currLev
    end
    listProto(nil, proto.main);
    Editor.cacheProto = cacheProto
    Editor.protoLevel = protoLevel
    Editor.levels = proto.levels
    Editor.header = proto.header -- Fixed path
    Event.invoke("onProtoLoaded")
    return Editor
end

---把已拆解區塊還原成正常區塊
---@return Prototype
function Editor:FinishEditing()
    -- Zip back to normal chunk
    local cacheProto, protoLevel = Editor.cacheProto, Editor.protoLevel
    Editor.cacheProto, Editor.protoLevel = nil, nil
    for level = #protoLevel, 1, -1 do
        local count = #cacheProto[level].func;
        if count > 0 then
            local newFunc = {}
            for fNum = 1, count do
                local id = cacheProto[level].func[fNum].id
                if not id or cacheProto[id] == nil then error("Logic error | At zipping proto") end
                local protoState = protoLevel[id].State
                if protoState == Editor.States.EXIST or protoState == Editor.States.EMPTY then
                    newFunc[#newFunc + 1] = cacheProto[id]
                else
                    Editor.levels = Editor.levels - 1
                end
                cacheProto[id], protoLevel[id] = nil, nil
            end
            cacheProto[level].func = newFunc
            local changed, result = Event.invoke("onCacheProtoZipped", level)
            if changed then
                cacheProto[level] = result
            end
        end
    end
    local cacheProto, protoLevel = cacheProto[1], nil
    Editor.States = nil
    return { header = Editor.header, main = cacheProto, levels = Editor.levels }
end

---分類編輯器類型, 並開啓選定的編輯器
---@param type number
---@return boolean|Prototype
function Editor:showEditor(type)
    if type == 1 then
        local msg, listen = Editor:CreateHeaderMessage()
        while true do
            listen = Alert(msg,
            Language.other.Back, nil, "(" .. Language.err.ReadOnly .. ")")
            if listen == 1 then
                return true
            end
        end
    elseif type == 2 then
        local ok, arg = Editor:displayProto(Editor.lastProtoNum or 1)
        if ok == true then
            Editor.lastProtoNum = arg
        end
        return ok, arg
    end
end

---詢問輸出文件路徑
---@return string
function Editor:showOutput()
    local name = "[C] " .. Config.Settings.inputFile:match('[^/]+$')
    local outPath = Config.Settings.inputFile:gsub('[^/]+$', '')
    local output;
    output = Prompt({
        Language.output.OutputFilePath,
        Language.output.OutputFileName,
        Language.output.KeepDebugInfo
    }, output or { outPath, name, Config.Settings.strip },
    { "path", "text", "checkbox" })
    if not output then
        -- TODO Ask to save this proto
        Toast(Language.err.UndoFailed)
        return
    else
        Config:addSettings("strip", output[3])
        :saveConfig()
        return output[1] .. "/" .. output[2]
    end
end

---取得區塊名稱
---@param level number
---@param prevPrefix string
---@return string
function Editor:getPrefix(level, prevPrefix)
    local prefix = ''
    level = level or 0
    prefix = prevPrefix or '%main'
    if level > 0 then
        prefix = prefix .. '-' .. level
    end
    return prefix
end

---以UI形式顯示區塊, 編輯器本體
---@param protoNum number @已拆解區塊的編號
---@return boolean|string
---@return number|Prototype
function Editor:displayProto(protoNum)
    local msg, listen, debugCount
    local proto = Editor.cacheProto[protoNum]
    Editor.currentLevel = protoNum
    :: WakeUp ::
    msg = Editor:CreateDisplayMessage(proto)
    debugCount = Editor:UpdateDebugCount(proto)
    listen = Choice({
        Language.proto.ProtoInfo[2],
        Language.proto.ConstantList .. " (" .. #proto.const .. ")",
        Language.proto.InstrList .. " (" .. #proto.instr .. ")",
        Language.proto.TypeDebug .. " (" .. debugCount .. ")",
        "================",
        Language.proto.ChangeProto, -- 6
        Language.proto.CompileProto, -- 7
        Language.plugin.Name, -- 8
        Language.settings.Name, -- 9
        Language.other.Back            -- 10
    }, nil, msg)

    if not listen or listen == 0 then -- Sleep mode ~
        while true do
            if gg.isVisible(true) then break end
        end
        SetUIVisibility(false)
        goto WakeUp
    elseif listen == 10 then -- Back
        return true, protoNum
    elseif listen == 9 then -- Settings
        Settings.main()
        goto WakeUp
    elseif listen == 8 then -- Plugins
        Plugin.showListUI()
        goto WakeUp
    elseif listen == 7 then -- Compile (Save and output)
        proto = Editor:FinishEditing()
        return "compile", proto
    elseif listen == 5 then -- Loop (As a hidden update option)
        goto WakeUp
    elseif listen == 1 then -- Function info
        local edit = proto
        local editList = {
            "linedefined",
            "lastlinedefined",
            "param",
            "vararg",
            "reg"
        }
        local cache = {
            edit.linedefined,
            edit.lastlinedefined,
            edit.param,
            (edit.vararg == 1),
            edit.reg
        }
        local CheckIsNotFloat = function(idx)
            local var = tonumber(cache[idx])
            if not var then return nil end
            cache[idx] = var
            return var % 1 == 0 or nil
        end
        while true do
            cache = Prompt({
                "(" .. Language.proto.TypeDebug .. ") " .. Language.proto.debug.LineDefined,
                "(" .. Language.proto.TypeDebug .. ") " .. Language.proto.debug.LastLineDefined,
                Language.proto.data.NumberOfArg .. " (0 - 255)",
                Language.proto.data.IsVararg,
                Language.proto.data.MaxStackSize .. " (2 - 255)"
            },
            cache,
            {
                "number", -- linedef
                "number", -- lastlinedef
                "number", -- param
                "checkbox", -- vararg
                "number"    -- reg
            })
            if cache then -- check input data
                local warnMsg = ""
                local isWarnDisplay = false
                -- de('s) | Language.other.Is[1]
                -- number invaild | Text:getError().InvaildNumber
                local warnList = {
                    Language.proto.debug.LineDefined,
                    Language.proto.debug.LastLineDefined,
                    Language.proto.data.NumberOfArg,
                    nil,
                    Language.proto.data.MaxStackSize,
                }
                local checkList = {
                    CheckIsNotFloat(1),
                    CheckIsNotFloat(2),
                    (CheckIsNotFloat(3) and cache[3] <= 255 and cache[3] >= 0) or nil,
                    true,
                    (CheckIsNotFloat(5) and cache[5] <= 255 and cache[5] >= 0) or nil
                }
                for i = 1, 5 do
                    if checkList[i] == nil then
                        if i ~= 1 and warnMsg ~= "" then
                            warnMsg = warnMsg .. "\n"
                        end
                        warnMsg = warnMsg .. "/!\\ " .. warnList[i] ..
                        Language.other.Is .. Language.err.InvaildNumber .. "!"
                        isWarnDisplay = true
                    end
                end
                if isWarnDisplay then
                    Alert(warnMsg, "")
                    goto contiue
                else -- no error
                    cache[4] = cache[4] == true and 1 or 0
                    for i = 1, 5 do
                        edit[editList[i]] = cache[i]
                    end
                end
            end
            do break end
            :: contiue ::
        end
        goto WakeUp
    elseif listen == 2 then -- Constant data
        local edit = proto.const
        local edited = {}
        local preview
        :: ConstEditDisplay ::
        preview = {}
        for i = 1, #edit do
            preview[i] = "<" .. (edited[i] and "*" or "-") .. i .. "> "
            local currConst = edit[i]
            if currConst.type == "nil" or
            currConst.type == "number" or
            currConst.type == "boolean" then
                preview[i] = preview[i] .. tostring(currConst[1])
            elseif currConst.type == "string" then
                local s = currConst[1]
                if #s > 10 then
                    s = s:sub(1, 10) .. "..."
                end
                for i = 1, 10 do
                    s = s:gsub(string.char(i), "\\" .. i)
                end
                preview[i] = preview[i] .. "\"" .. s .. "\" (" .. #currConst[1] .. ")"
            end
        end
        local sel = Choice(preview, nil, Language.proto.ConstantList .. "\n" ..
        Language.proto.const.CountConstant .. ": " .. #edit ..
        "\n" .. Language.tips.PressBack)
        if sel == 0 or not sel then
            -- Show editor
            goto WakeUp
        elseif sel > 0 then
            if edit[sel].type == "string" then
                -- Show edit tips: Externl File edit mode
                if not Config.Settings.NoConstTips then
                    local listen = Alert(Language.tips.EditConst,
                    Language.other.Yes[3],
                    nil,
                    Language.other.Dismiss)
                    if listen == 3 then
                        Config:addSettings("NoConstTips", true):saveConfig()
                    end
                end
                if not Config.Settings.ConstEditPath then
                    local listen
                    :: ConstEditPath ::
                    listen = Prompt({
                        Language.settings.ConstantEditPath
                    }, listen or { "/sdcard" },
                    { "path" })
                    if not listen then -- Back
                        goto ConstEditDisplay
                    else -- Test & Save path
                        local randName = math.random(11111111, 99999999)
                        local testPath = listen[1] .. "/$file" .. randName .. ".txt"
                        local test = io.open(testPath, "w")
                        if not test then
                            Alert(Language.err.InvaildPath, "")
                            goto ConstEditPath
                        end
                        test:close()
                        os.remove(testPath)
                        Config:addSettings("ConstEditPath", listen[1]):saveConfig()
                    end
                end
                local lastText = edit[sel][1];
                local randName = math.random(11111111, 99999999)
                local fileName = "Const" .. randName .. ".txt"
                local editFilePath = Config.Settings.ConstEditPath .. "/" .. fileName
                local steam = io.open(editFilePath, "w+")
                steam:write(edit[sel][1])
                steam:close()
                SetUIVisibility(true)
                while true do -- Edit state
                    if IsUIVisibility(true) then
                        SetUIVisibility(false)
                        local listen = Alert(Language.other.ConstCreatedHead .. ":\n\"" .. Config.Settings.ConstEditPath ..
                        "\" (" .. fileName .. ")\n" .. Language.other.ConstCreatedTail, Language.other.Done, "Clear",
                        Language.other.DropEdit)
                        if listen == 1 then -- Finish Editing
                            local testFile = io.open(editFilePath)
                            if not testFile then
                                edit[sel][1] = ""
                            else
                                edit[sel][1] = testFile:read("*a")
                            end
                            edited[sel] = true
                        elseif listen == 2 then -- Clear/Delete This string
                            edit[sel][1] = ""
                            edited[sel] = true
                        end
                        if listen > 0 then
                            os.remove(editFilePath)
                            local testBak = io.open(editFilePath .. ".bak")
                            if testBak then
                                os.remove(editFilePath .. ".bak")
                            end
                            break
                        end
                    elseif Config.Settings.ListenFile then
                        -- Listen file, if edited then open alert
                        local steam = io.open(editFilePath)
                        if steam then
                            local currText = steam:read("*a")
                            gg.sleep(100)
                            if currText ~= lastText then
                                lastText = currText
                                SetUIVisibility(true)
                            end
                        end
                    end
                end
            elseif edit[sel].type == "number" then
                local num
                :: EditNumber ::
                num = Prompt({ edit[sel][1] }, num or { edit[sel][1] }, { "number" })
                if num then
                    local testNum = tonumber(num[1])
                    if not testNum then
                        Alert(Language.err.InvaildNumber, "")
                        goto EditNumber
                    end
                    edit[sel][1] = tonumber(num[1])
                    edited[sel] = true
                end
            elseif edit[sel].type == "boolean" then
                edit[sel][1] = not edit[sel][1]
                if edited[sel] then
                    edited[sel] = not edited[sel]
                else
                    edited[sel] = true
                end
            elseif edit[sel].type == "nil" then
                Alert(Language.err.UnEditable)
            end
            goto ConstEditDisplay
        end
    elseif listen == 3 then -- Instr data
        local edit = proto.instr
        :: ReFresh ::
        local preview = {}
        local level = { "A", "B", "C", "Bx", "sBx", "Ax" }
        for i = 1, #edit do
            preview[i] = "[" .. i .. "] "
            local currInstr = edit[i]
            local displayType = "(|"
            preview[i] = preview[i] .. currInstr[1] .. "\n"
            for _, iType in ipairs(level) do
                if currInstr[2][iType] then
                    preview[i] = preview[i] .. currInstr[2][iType] .. " "
                    displayType = displayType .. iType .. "|"
                end
            end
            preview[i] = preview[i] .. displayType .. ")"
        end
        local listen
        while true do
            :: UnEdit ::
            listen = Choice(preview, nil, Language.proto.InstrList .. "\n" ..
            Language.proto.instr.CountInstr .. ": " .. #edit ..
            "\n" .. Language.tips.PressBack)
            if not listen then
                break
            elseif listen > 0 then
                local cache = edit[listen]
                local isEdit
                -- Type
                :: ReFreshOP ::
                local isRefresh
                local typeDisplay = ""
                local typeDataDisplay = "i"
                for _, type in ipairs(level) do
                    if cache[2][type] then
                        typeDataDisplay = typeDataDisplay .. type
                        typeDisplay = typeDisplay .. type .. ": " .. cache[2][type] .. " "
                    end
                end
                -- End of Type
                local concat = table.concat
                while true do
                    if isRefresh then
                        isEdit = true
                        goto ReFreshOP
                    end
                    :: BackSingle ::
                    local checkEdit = Choice({
                        Language.proto.instr.Name[1] .. ": " .. cache[1],
                        Language.other.Type .. ": " .. typeDataDisplay,
                        typeDisplay
                    })
                    if not checkEdit then
                        if isEdit then
                            goto ReFresh
                        end
                        goto UnEdit
                    elseif checkEdit == 1 then -- Change Instr
                        -- Current edit: cache[1] (OP name)
                        local opNameList = Bytecode:getOPNameList()
                        local opNumList = Bytecode:getOPNumList()
                        local opName = cache[1]
                        if opName == "UNKNOWN" then
                            Toast(Language.err.UnEditable)
                            goto BackSingle
                        end
                        local opNumber = opNameList[opName]
                        local opType = concat(opNumList[opNumber][opName], "")
                        local listenList = {}
                        for i = 0, #opNumList do
                            local Name, iType = next(opNumList[i])
                            listenList[i + 1] = i .. " " .. Name .. " (i" .. concat(iType, "") .. ")"
                        end
                        local listen = Choice(listenList, nil,
                        Language.proto.instr.CountInstr .. ": " .. opNumber .. " " .. opName ..
                        " (" .. opType .. ")\n\n" .. Language.proto.instr.SelectNewInstr)
                        -- After changed op codes, we need to change the args too.
                        if listen then
                            -- listen - 1 = op number
                            local targetName, targetType = next(opNumList[listen - 1])
                            if targetName == opName then
                                goto BackSingle
                            end
                            local testType = concat(targetType, "")
                            if testType ~= opType and Alert(Language.tips.ForceDataTypeChangeWarn,
                            Language.other.Yes[1], nil, Language.other.No[1]) == 1 then
                                -- Set a warning to notify user.
                                cache[2] = Bytecode:changeType(cache, targetType)
                                cache.type = targetType
                                isRefresh = true
                            elseif testType == opType then
                                cache[1] = targetName
                                isRefresh = true
                            end
                        end
                    elseif checkEdit == 2 then -- Change Type
                        Toast(Language.err.UnEditable)
                        goto BackSingle
                    elseif checkEdit == 3 then -- Edit Data
                        local typeText = concat(cache.type, "")
                        local text = Language.proto.instr.Name[1] .. ": " .. cache[1] .. "\n" ..
                        Language.other.Type .. ": " .. typeText .. "\n\n"
                        local display = {}
                        local input = {}
                        local types = {}
                        if #cache.type > 1 then
                            for i = 2, #cache.type + 1 do
                                display[i] = cache.type[i]
                                input[i] = cache[2][display[i]]
                                types[i] = "number"
                            end
                        end
                        display[1] = text .. cache.type[1]
                        input[1] = cache[2][cache.type[1]]
                        types[1] = "number"
                        local listen
                        :: ReEdit ::
                        listen = Prompt(display, listen or input, types)
                        if listen then
                            for i = 1, #listen do
                                local num = tonumber(listen[i])
                                if num == nil then
                                    Alert(Language.err.InvaildNumber .. ": `" .. listen[i] .. "`", "")
                                    isRefresh = false
                                    goto ReEdit
                                else
                                    cache[2][cache.type[i]] = num
                                    isRefresh = true
                                end
                            end
                        end
                    end
                end
            end
        end
        goto WakeUp
    elseif listen == 4 then -- Debug info
        local edit = proto.extra
        local listen
        local CheckIsListEmpty = function(list)
            if not list or (list and #list < 0) then
                Toast(Language.err.ListIsEmpty)
                return true
            end
            return false
        end
        :: UpdateList ::
        local preview = {}
        local s = edit.source or ""
        if #s > 10 then
            s = s:sub(1, 10) .. "..."
        end
        for i = 1, 10 do
            s = s:gsub(string.char(i), "\\" .. i)
        end
        preview[1] = "Source " .. Language.other.Name .. " (\"" .. s .. "\") [" .. #(edit.source or "") .. "]"
        preview[2] = "Linenumber " .. Language.other.List .. " (" .. (edit.linenumber and #edit.linenumber or 0) .. ")"
        preview[3] = "Local " .. Language.other.Name .. Language.other.List .. " (" .. (edit.locals and #edit.locals or 0) .. ")"
        preview[4] = "Upval " .. Language.other.Name .. Language.other.List .. " (" .. (#proto.upvalue or 0) .. ")"
        :: Back_UnUpdated ::
        listen = Choice(preview, nil, Language.proto.TypeDebug .. " (" .. Editor:UpdateDebugCount(proto) .. ")\n" ..
        "\n" .. Language.tips.PressBack)
        if not listen then
            goto WakeUp
        elseif listen == 1 then -- Source name
            local source = edit.source
            local text = Prompt({ source }, { source }, { "text" })
            if text then
                edit.source = text[1]
            end
        elseif listen == 2 then -- Line number
            if CheckIsListEmpty(edit.linenumber) then
                goto Back_UnUpdated
            end
            local LN = edit.linenumber
            local listen
            :: ReFresh ::
            local preview = {}
            for i = 1, #LN do
                preview[i] = "[" .. i .. "] " .. LN[i]
            end
            while true do
                listen = Choice(preview, nil, Language.proto.TypeDebug ..
                " > Line Number " ..
                "(" .. #preview .. ")\n" ..
                "\n" .. Language.tips.PressBack)

                if not listen then
                    break
                elseif listen > 0 then
                    -- edit state
                    local number = Prompt({ "" }, { LN[listen] }, { "number" })
                    if number then
                        LN[listen] = number[1]
                        goto ReFresh
                    end
                end
            end
        elseif listen == 3 then -- Local name
            if CheckIsListEmpty(edit.locals) then
                goto Back_UnUpdated
            end
            local LCLS = edit.locals
            local listen
            :: ReFresh ::
            local preview = {}
            for i = 1, #LCLS do
                local new_LCLS = LCLS[i]
                local s = new_LCLS.name or ""
                if #s > 10 then
                    s = s:sub(1, 10) .. "..."
                end
                for i = 1, 10 do
                    s = s:gsub(string.char(i), "\\" .. i)
                end
                preview[i] = "[" .. (i - 1) .. "] " .. Language.other.Name .. ": " .. s ..
                " (" .. #new_LCLS.name .. ")\n" .. "Start at line (" ..
                new_LCLS.startpc .. "), end at line (" .. new_LCLS.endpc
                .. ")"
            end
            while true do
                listen = Choice(preview, nil,
                Language.proto.TypeDebug ..
                " > Local " .. Language.other.Name ..
                " (" .. #preview .. ")\n" ..
                "\n" .. Language.tips.PressBack)

                if not listen then
                    break
                elseif listen > 0 then
                    -- edit state
                    local cache = {
                        LCLS[listen].name,
                        LCLS[listen].startpc,
                        LCLS[listen].endpc
                    }
                    local locals = Prompt({
                        Language.other.Name,
                        Language.proto.debug.LineDefined,
                        Language.proto.debug.LastLineDefined
                    }, cache, {
                        "text",
                        "number",
                        "number"
                    })
                    if locals then
                        LCLS[listen] = {
                            name = locals[1],
                            startpc = locals[2],
                            endpc = locals[3]
                        }
                        goto ReFresh
                    end
                end
            end
        elseif listen == 4 then -- Upvalue name
            if CheckIsListEmpty(proto.upvalue) and CheckIsListEmpty(edit.upval) then
                goto Back_UnUpdated
            end
            local listen
            :: ReFresh ::
            local edit_1 = proto.upvalue; -- Upval
            local edit_2 = proto.extra.upval or {}; -- Upval (Debug)
            local preview = {}
            for i = 1, #edit_1 do
                local name = edit_2[i] or "(nil)"
                local nameLen = edit_2[i] and #edit_2[i] or -1
                if #name > 10 then
                    name = name:sub(1, 10) .. "..."
                end
                for i = 1, 10 do
                    name = name:gsub(string.char(i), "\\" .. i)
                end
                preview[i] = "[" .. (i - 1) .. "] " .. Language.other.Name ..
                ": " .. name .. " (" .. nameLen .. ")\n" ..
                Language.proto.upval.IsInStack .. ": " ..
                Language.other[(edit_1[i].instack and "Yes" or "No")][1]
                .. " | " .. Language.other.Position .. ": " .. edit_1[i].position
            end
            while true do
                listen = Choice(preview, nil, Language.proto.TypeDebug ..
                " (" .. #preview .. ")\n" ..
                "\n" .. Language.tips.PressBack)
                if not listen then
                    break
                elseif listen > 0 then
                    local isDebugUpvalExist = edit_2[listen] ~= nil
                    local upvals;
                    if isDebugUpvalExist then -- Add name
                        upvals = Prompt({
                            Language.other.Name,
                            Language.proto.upval.IsInStack,
                            Language.other.Position
                        },
                        {
                            edit_2[listen],
                            edit_1[listen].instack,
                            edit_1[listen].position
                        }, {
                            "text",
                            "checkbox",
                            "number"
                        })
                    else -- No name provided
                        upvals = Prompt({
                            Language.proto.upval.IsInStack,
                            Language.other.Position
                        },
                        {
                            edit_1[listen].instack,
                            edit_1[listen].position
                        }, {
                            "checkbox",
                            "number"
                        })
                    end
                    if upvals then
                        if isDebugUpvalExist then -- update both
                            edit_2[listen] = upvals[1]
                            edit_1[listen].instack = upvals[2]
                            edit_1[listen].position = upvals[3]
                        else -- normal only
                            edit_1[listen].instack = upvals[1]
                            edit_1[listen].position = upvals[2]
                        end
                        goto ReFresh
                    end
                end
            end
            -- end of upval
        end
        goto UpdateList
    elseif listen == 6 then -- switch block
        -- 1. Count instr
        -- 2. Get protoLevel's data
        local display = {}
        for num, info in ipairs(Editor.protoLevel) do
            local numInstr
            if info.State == Editor.States.EMPTY then
                numInstr = Language.other.Empty
            elseif info.State == Editor.States.DELETED then
                numInstr = "(" .. Language.other.Deleted .. ")"
            else
                numInstr = #Editor.cacheProto[num].instr
            end
            display[num] = "[" .. info.DEFAULT .. "] " .. info.SSTOOL .. "\n"
            .. Language.proto.instr.CountInstr .. ": " .. numInstr
        end
        local listen = Choice(display, protoNum, Language.proto.funcs.CountProtos .. ": " .. Editor.levels ..
        "\n(" .. Language.tips.PressBack .. ")")
        if not listen or listen == protoNum then
            goto WakeUp
        elseif listen > 0 then
            return Editor:displayProto(listen)
        end
    end
end

---創建Lua開頭信息
---@return string
function Editor:CreateHeaderMessage()
    local isOfficalBool = Language.other[Editor.header.RIO == 0 and "Yes" or "No"]
    local endianBool = Language.other[Editor.header.endian == true and "Yes" or "No"]
    local isUsingInt = Language.other[Editor.header.float == 1 and "Yes" or "No"]
    return "" ..
    Language.proto.header.LuaSign_Ver .. " (" .. Editor.header.sign .. ")\n" ..
    Language.proto.header.IsOffical .. " = " .. isOfficalBool[1] .. "\n" ..
    Language.proto.header.IsEndian .. " = " .. endianBool[1] .. "\n" ..
    Language.proto.header.SizeofInt .. " (" .. Editor.header.int .. ")\n" ..
    Language.proto.header.SizeofT .. "  (" .. Editor.header.sizet .. ")\n" ..
    Language.proto.header.SizeofInstr .. " (" .. Editor.header.instr .. ")\n" ..
    Language.proto.header.SizeofNumber .. " (" .. Editor.header.numlen .. ")\n" ..
    Language.proto.header.IsUsingInt .. " = " .. isUsingInt[1] .. "\n" ..
    Language.proto.header.LuaTail .. " (" .. Editor.header.tail .. ")"
end

---更新調試信息數量
---@param proto CacheProto
---@return number
function Editor:UpdateDebugCount(proto)
    local list = { "source", "linenumber", "locals", "upval" }
    local count = 0
    for _, type in ipairs(list) do
        if proto.extra[type] and #proto.extra[type] > 0 or #list == _ then
            if #list == _ then
                -- Check upvalue
                if #proto.upvalue > 0 then
                    count = count + 1
                elseif proto.extra[type] and #proto.extra[type] > 0 then
                    count = count + 1
                end
            else
                count = count + 1
            end
        end
    end
    return count
end

---創建區塊函數信息
---@param proto CacheProto
---@return string
function Editor:CreateDisplayMessage(proto)
    local typeDebug = "(" .. Language.proto.TypeDebug .. ") "
    local varargBool = Language.other[proto.vararg > 0 and "Yes" or "No"]
    return "" ..
    "[" .. proto.level .. "] " .. Editor:getPrefix(proto.level) .. "\n" ..
    typeDebug .. Language.proto.debug.LineDefined .. ": " .. proto.linedefined .. " | " ..
    Language.proto.debug.LastLineDefined .. ": " .. proto.lastlinedefined .. "\n" ..
    Language.proto.data.NumberOfArg .. ": " .. proto.param .. "\n" ..
    Language.proto.upval.NumberOfUpvalue .. ": " .. #proto.upvalue .. "\n" ..
    Language.proto.instr.NumberOfInstr .. ": " .. #proto.instr .. "\n" ..
    Language.proto.data.MaxStackSize .. ": " .. proto.reg .. "\n" ..
    Language.proto.data.IsVararg .. ": " .. varargBool[1] .. "\n" ..
    Language.proto.const.NumberOfConstant .. ": " .. #proto.const .. "\n" ..
    Language.proto.funcs.NumberOfProtos .. ": " .. #proto.func
end

function Editor:init()
    -- Register event
    Event.register("onCacheProtoZipped")

    Event.register("onProtoLoaded")
end