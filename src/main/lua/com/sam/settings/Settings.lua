--[[    Functions:
        * OK (Currently)
        
--]]
---@class Settings
Settings = {}


local UI
local avalibleSettings = {}
local UIText = {}

function Settings:init()
    for name, _ in pairs(Config.Settings) do
        avalibleSettings[#avalibleSettings + 1] = name
    end
    local count = #avalibleSettings
    for i = 1, count do
        if Language.settings[avalibleSettings[i] .. "Name"] == nil then
            print("Warning: `" .. tostring(avalibleSettings[i]) .. "` has no translate name?!")
            UIText[i] = '"' .. avalibleSettings[i] .. '"'
        else
            UIText[i] = Language.settings[avalibleSettings[i] .. "Name"]
        end
    end
end

function Settings:refreshDisplay()
    local count = #UIText
    local tempUI = {}
    for i = 1, count do
        tempUI[i] = UIText[i] .. ": " .. Settings:displaySetting(avalibleSettings[i])
    end
    UI = tempUI
end

---@param int number
---@param var any
function Settings:refreshEdited(int, var)
    UI[int] = UIText[int] .. ": " .. Settings:displaySetting(var, true)
end

---@param configVar number
---@return any
function Settings:edit(configVar, isEdited)
    local var
    if isEdited then var = configVar
    else var = Config.Settings[configVar] end
    if type(var) == "string" then
        local types = "text"
        if not isEdited then
            if configVar:find(".+Path$") then
                types = "path"
            elseif configVar:find(".+File$") then
                types = "file"
            end
        end
        local edit = Prompt({ var }, { var }, { types })
        if not edit then
            return var
        else
            return edit[1]
        end
    elseif type(var) == "boolean" then
        return not var
    end
end

---@param configVar number|any
---@param isEdited boolean
---@return string
function Settings:displaySetting(configVar, isEdited)
    local var, default;
    if isEdited then
        var, default = configVar, configVar
    else
        var = Config.Settings[configVar]
        default = Config.defaultSettings[configVar]
    end
    if type(var) == "boolean" then
        return var == true and Language.other.Yes[1] or Language.other.No[1]
    elseif type(var) == "string" then
        local tmpStr = var
        if #var > 10 then
            tmpStr = var:sub(0, 7) .. "..."
        else
            return "\n(" .. tmpStr .. ")"
        end
    elseif var == nil then
        if default ~= nil then
            return default
        else
            return "null"
        end
    end
    return var
end

---@param edited table
function Settings:saveConfig(edited)
    local flag
    for i in pairs(edited) do
        if Config.Settings[avalibleSettings[i]] ~= edited[i] then
            Config:addSettings(avalibleSettings[i], edited[i])
            flag = true
        end
    end
    if flag then
        Config:saveConfig()
    end
end

function Settings:main()
    local edited = {}
    Settings:refreshDisplay()
    while true do
        local sel = Choice(UI, nil, Language.settings.Name);
        if not sel or sel == 0 then
            Settings:saveConfig(edited)
            break
        elseif sel > 0 then
            if avalibleSettings[sel] == "lang" then -- 語言
                Language.reSelectLang()
                SetUIVisibility(false)
            else
                local orig, edit
                if edited[sel] ~= nil then
                    orig = edited[sel]
                    edit = Settings:edit(edited[sel], true)
                else
                    orig = Config.Settings[avalibleSettings[sel]]
                    edit = Settings:edit(avalibleSettings[sel])
                end
                if orig ~= edit then
                    edited[sel] = edit
                    Settings:refreshEdited(sel, edit)
                end
            end
        end
    end
end